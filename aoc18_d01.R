library(tidyverse)

# Part 1 -------------------------------------------------------------------------------------------

d01_d1 <- read_csv("data/d01_input.txt", col_names = FALSE)

d01_f1 <- function(c) {
    # c %>% tally(wt = X1)
    c %>% sum()
}

d01_d1 %>% pull(X1) %>% d01_f1()

# Part 2 -------------------------------------------------------------------------------------------

d01_f2 <- function(c, n) {
    print(str_glue("Dla n = {n}:"))
    tibble(X1 = rep(c, n)) %>%
        rownames_to_column() %>%
        mutate(S = cumsum(X1), rowname = as.integer(rowname)) %>%
        janitor::get_dupes(S) %>%
        group_by(S) %>%
        slice(2) %>%
        arrange(rowname) %>%
        pull(S) %>%
        .[[1]] %>%
        str_glue()
}

d01_f3 <- possibly(d01_f2, otherwise = str_glue("Nie ma powtórzeń"))

d01_f4 <- function() {
    n <- 1
    while (TRUE) {
        d01_d1 %>% pull(X1) %>% d01_f3(n) %>% print()
        # Sys.sleep(0.5)
        n <- n+1
    }
}

c(+1, -1) %>% d01_f3(2)
c(+3, +3, +4, -2, -4) %>% d01_f3(2)
c(-6, +3, +8, +5, -6) %>% d01_f3(3)
c(+7, +7, -2, -7, -4) %>% d01_f3(3)

d01_d1 %>% pull(X1) %>% d01_f3(24)

d01_f4()
